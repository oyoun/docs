Donnez-moi un exemple simple à comprendre&nbsp;!
-------------------------------------------

Tristan a des choses à dire sur ce qu’il pense des GAFAM (Google, Apple,
Amazon, Facebook, Microsoft), et de l’utilisation qu’ils font de nos
données personnelles. D’ailleurs il est régulièrement invité pour en
parler car son expertise sur le sujet est reconnue. Il a donc décidé de
rassembler ses idées [dans un
livre](http://cfeditions.com/surveillance/). Après plusieurs mois de
rédaction et avoir pris bonne note des retours qui lui ont été faits par
les lectrices et lecteurs de son blog, il a trouvé une maison d’édition
proche de ses valeurs prête [à publier son
livre](http://cfeditions.com/surveillance/). Le jour tant attendu du
lancement de son ouvrage approche, mais afin de pouvoir s’organiser, il
décide de créer un formulaire en ligne invitant à s’inscrire les
personnes qui souhaitent venir.

### Framaforms à la rescousse

Première étape, l’inscription. Rien d’extraordinaire de ce côté-là.
Tristan se rend sur [https://framaforms.org](framaforms.org) et clique
sur «&nbsp;Créer un compte&nbsp;». Il saisit alors un login, son adresse email, et
répond à la question servant à s’assurer qu’il n’est pas un
robot-spammeur (pour info, la réponse est «&nbsp;framaforms&nbsp;»&nbsp;:P ) Il reçoit
quelques secondes plus tard un email provenant du site lui demandant de
cliquer sur un lien pour terminer son inscription. Il clique dessus et
peut alors choisir son mot de passe et quelques informations
complémentaires.

![Création d’un
compte](images/framaforms_01_compte.png)

Voilà, son compte est créé et validé, il peut commencer son formulaire&nbsp;!

### Création du formulaire

Il clique sur «&nbsp;Créer un formulaire&nbsp;». Le site lui demande alors de
remplir les informations de base, comme l’intitulé («&nbsp;Inscription au
lancement de mon livre&nbsp;»).

![Création de
formulaire](images/framaforms_02_creation.png)

Il choisit aussi de mettre en ligne une description et une image qui
rappelleront aux gens de quoi il s’agit.

![Ajout d’une
description](images/framaforms_03_description.png.png)

Comme date d’expiration, Tristan choisit une date 15 jours après
l’événement. Il aura de toutes façons récupéré toutes les informations
d’ici là, et inutile d’encombrer les serveurs avec un formulaire dont
les informations n’auront plus d’intérêt quelques jours plus tard. Comme
Tristan est un type sympa, il se dit que son formulaire pourra servir à
d’autres plus tard, et décide donc de faire de son formulaire un «
modèle&nbsp;». Cela signifie que son formulaire se retrouvera parmi les
multiples modèles de formulaires dont d’autres utilisateurs pourront
s’inspirer et qu’ils pourront surtout «&nbsp;cloner&nbsp;» d’un seul clic, leur
faisant gagner un temps précieux. Il décide de nommer ce formulaire «
Modèle de formulaire d’inscription à un événement&nbsp;».

![Options de
création](images/framaforms_04_options.png)

Il passe alors à l’étape de la construction de son formulaire.

### Conception du formulaire {#magicdomid48}

C’est simple et rapide&nbsp;: il suffit de glisser-déposer les champs, puis
de cliquer dessus pour éditer les informations qui seront affichées. Il
commence donc par un champ texte pour le nom ou le pseudo. Il clique sur
le crayon et complète les informations souhaitées. Il en profite
d’ailleurs pour rendre ce champ obligatoire.

![Ajout d’un
champ](images/framaforms_04_pseudo.gif)

Comme il souhaite savoir comment les inscrits ont entendu parler de son
ouvrage, il utilise alors un champ «&nbsp;boutons radio&nbsp;». Et remplit 3
champs «&nbsp;Par l’auteur&nbsp;», «&nbsp;Par l’éditeur&nbsp;», «&nbsp;Autre&nbsp;».

![Ajout de boutons de
sélection](images/framaforms_05_boutons_radios.png)

Afin de savoir avec combien de livres son éditeur doit venir le jour J,
il décide de poser la question sous forme d’une simple case à cocher.

![Ajout d’une case à
cocher](images/framaforms_06_case_a_cocher.png)

Enfin, il décide d’ajouter, à la demande de son éditeur, un champ email
pour les personnes qui souhaiteraient être tenues au courant de
l’actualité de ce dernier. Aucun problème, un dernier glisser-déposer et
c’est réglé.

![Ajout d’un champ
courriel](images/framaforms_07_courriel.png)

Et voilà, il enregistre, et son formulaire est prêt à être diffusé&nbsp;! Il
peut le visualiser et le tester en cliquant sur «&nbsp;Voir&nbsp;»

![Prévisualisation](images/framaforms_07b_visu1-753x1024.png)

### Options

Bon, jusqu’ici ça ne lui a pris que 5 minutes chrono, mais Tristan se
dit que ça mérite un peu de peaufinage. C’est un jour important après
tout&nbsp;! D’abord, il retourne modifier son formulaire et décide de
rajouter un champ texte «&nbsp;Pouvez-vous m’en dire plus&nbsp;?&nbsp;» qui ne
s’affichera QUE si le participant coche la case «&nbsp;Autre&nbsp;». Il ajoute ce
champ sous les boutons radio et enregistre son formulaire.

![Ajout d’un nouveau champ qui ne sera affiché que si un autre est
coché](images/framaforms_08_plus_d_infos.png)

Puis, il clique sur «&nbsp;champs conditionnels&nbsp;» et sélectionne les menus de
façon à formuler la phrase «&nbsp;Si Comment avez-vous entendu parler de cet
événement est Autre alors Pouvez-vous m’en dire plus&nbsp;? est affiché&nbsp;»,
puis enregistre. Simple&nbsp;!

![Choix du champ à
afficher](images/framaforms_09_conditionnels.png)

Le résultat est concluant&nbsp;:

![Champ s’affichant sous
condition](images/framaforms_10_conditionnel_anim.gif)

Par ailleurs, il se dit qu’il aimerait bien recevoir un mail à chaque
réponse. Il se rend dans l’onglet «&nbsp;courriels&nbsp;» et ajoute un «&nbsp;courriel
standard&nbsp;». Pour adresse courriel du destinataire, il met la sienne. Il
parcourt les autres champs, mais les valeurs par défaut lui conviennent,
et il décide donc de valider.

![Ajout d’une adresse email pour recevoir un message à chaque
participation.](images/framaforms_11_courriels.png)

Dernière modification, cosmétique, dans l’onglet «&nbsp;Modifier&nbsp;», tout en
bas, il choisit un autre thème, plus adapté aux smartphones que le thème
par défaut (il faut dire que les amis de Tristan sont très connectés).
Il enregistre encore une fois.

![Choix d’un thème
différent](images/framaforms_12_theme.png)

Voilà, son formulaire peut être diffusé&nbsp;!

### Diffusion

En se rendant sur l’onglet «&nbsp;Partager&nbsp;», Tristan voit une option pour
partager son formulaire sur les réseaux sociaux. Il a supprimé son
compte Facebook il y a très longtemps, parce que l’entreprise modifiait
sans cesse ses conditions d’utilisation, de plus en plus abusives. Par
contre Tristan a un compte Diaspora* sur
[Framasphère](https://framasphere.org), le pod du réseau social loyal et
respectueux de vos données, géré par l’association Framasoft (le pod,
pas le réseau&nbsp;:P). Et il est aussi très présent sur Twitter (100 000
abonnés tout de même). Il publie donc l’annonce du lancement de son
livre sur ces deux réseaux. Il a même le code HTML qui lui permet
d’afficher ce formulaire directement embarqué sur son site. Il envoie
aussi l’adresse de son formulaire à ses contacts par email.

![Possibilités offertes pour partager son
formulaire](images/framaforms_13_partager.png)

Les dés sont jetés.

### Collecte, analyse et téléchargement des données

Quelques jours plus tard, Tristan se connecte sur Framaforms et peut
retrouver son formulaire via le bouton «&nbsp;Mes formulaires&nbsp;». Il clique
sur son formulaire, puis sur «&nbsp;Résultats&nbsp;». Il peut alors voir le nombre
de réponses et visualiser chacune d’entre elles en situation (et
supprimer les tests qu’il avait faits au début).

![Liste des participations (possibilité de
visualiser/supprimer)](images/framaforms_14_resultats1.png)

Il peut aussi sélectionner l’onglet «&nbsp;Analyse&nbsp;» pour afficher des
graphiques des réponses.

![Analyse et
graphiques](images/framaforms_15_resultats2.png)

L’onglet «&nbsp;Tableau&nbsp;» permet, lui, d’avoir une vision globale des
réponses (pratique pour les formulaires ne comportant pas trop
d’éléments.

![Détails des
participations](images/framaforms_16_resultats3.png)

Enfin, il peut bien entendu télécharger les résultats au format .csv
pour importer les informations brutes dans, par exemple, LibreOffice
Calc (son tableur préféré).

![Téléchargement des
résultats](images/framaforms_17_resultats4.png)

### Conclusion

Tristan a donc créé un formulaire en quelques minutes, qui plus est en
étant certain que les données des réponses des participants n’iront pas
nourrir l’ogre Google.

![Formulaire final tel que vu par les
utilisateurs](images/framaforms_18_visu2-1024x1002.png)

Il n’en a pas eu l’utilité, mais de nombreuses autres options étaient
disponibles. Par exemple il aurait pu ajouter un champ pour demander
l’âge des participants, avec une vérification automatique que la valeur
saisie était bien un nombre compris entre 7 et 97 ans. Ou renvoyer
automatiquement le participant sur une page de remerciements sur son
blog une fois le formulaire rempli. Ou limiter le nombre de places aux
100 premiers répondants. Ou&hellip;


