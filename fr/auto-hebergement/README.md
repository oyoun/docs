# L’auto-hébergement facile
## *Puisqu’on n’est jamais mieux servi que par soi-même, créez l’internet que vous voulez*

Auteur : Xavier Cartron

Copyright : mai 2016

Licence: [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/)

-------

À propos…
====================

Ce document est distribué sous la licence libre [CC-BY-SA](https://creativecommons.org/licenses/by-sa/4.0/).
Si vous l’avez apprécié, n’hésitez pas à donner en échange un petit
quelque chose à [framasoft](https://soutenir.framasoft.org/).

