Divers
======

Que faire quand ça ne marche pas?
---------------------------------

En installant votre serveur, quelque chose ne fonctionne pas comme
prévu. Pas de panique, vous allez pouvoir identifier la source du
problème en parcourant les fichiers de “logs”.

Ces fichiers enregistrent l’activité de votre serveur, et sont
disponibles dans le dossier `/var/log`. Regardez donc à l’intérieur afin
d’avoir des indices sur l’origine du problème rencontré.

Notez que vous pouvez observer les logs défiler au fur et à mesure
qu’ils sont enregistrés grâce à la commande suivante :

```
tail -f /var/log/fichier-de-log
```

C’est très pratique pour tester votre serveur.

Voici quelques exemples de fichiers de log :

-   `/var/log/messages` : C’est le fichier contenant les messages
    système.
-   `/var/log/syslog` : Comme pour /var/log/messages mais en plus
    complet.
-   `/var/log/nginx/error.log` : Contient les messages d’erreur de
    nginx.
-   `/var/log/mail.err` : Contient les messages d’erreur du serveur mail
    (postfix).

Surveiller votre serveur avec Logwatch
--------------------------------------

Logwatch vous permettra de recevoir des mails quotidiens sur l’état de
votre serveur. Il se configure dans le fichier
`/usr/share/logwatch/default.conf/logwatch.conf`

Recevoir un mail lorsque quelqu’un se connecte
----------------------------------------------

Pour être averti dès que quelqu’un se connecte en ssh, créez le script
suivant à enregistrer sous `/etc/ssh/sshrc` :

```
#!/bin/sh
# source: http://blog.uggy.org/post/2009/06/05/...
DATE=`date "+%d.%m.%Y--%Hh%Mm"`
IP=`echo $SSH_CONNECTION | awk '{print $1}'`
REVERSE=`dig -x $IP +short`
echo "Connexion de $USER sur $HOSTNAME
IP: $IP
ReverseDNS: $REVERSE
Date: $DATE

" | mail -s "Connexion de $USER sur $HOSTNAME" moi@laposte.net
```

Foire aux questions – FAQ
-------------------------

> Il paraît que pour un serveur, on peut utiliser un vieux PC. Mais
quelle taille de disque dur est nécessaire au minimum?

Ça dépend. La réponse ne sera pas la même selon ce que vous voulez
faire du serveur.

Pour un petit site web, quelques Go suffiront amplement.

Si c’est un site contenant images et vidéos à gogo, alors il faudra
un espace plus conséquent.

Si c’est pour faire une seedbox ou un mediacenter, alors il faut
compter encore plus.

Dans la majorité des cas, entre 10G et 20G seront bien assez.

> Et pour le processeur, quelle puissance au moins?

Là aussi, ça dépend :). Pour un simple site avec quelques
visites par jour, peu de puissance est nécessaire.

S’il y a du php, il faut alors une puissance un peu plus élevée.

Enfin, si le serveur propose de la messagerie instantanée, un site
avec php, des mails…Vous l’aurez compris, il faudra encore une
puissance plus grande.

Il en va de même pour la mémoire vive d’ailleurs.

Notez qu’un simple raspberry pi, donc très peu puissant, est souvent
assez pour un serveur auto-hébergé.

> Admettons que on site soit sur `http://serveur-à-moi`, comment
est-il visible sur internet par d’autres personnes ?

Lorsque quelqu’un tape l’adresse de votre site dans son navigateur,
c’est traduit en une série de chiffres qui permet de retrouver votre
serveur. Il s’agit du fonctionnement des DNS, expliqué au paragraphe
[DNS](prerequis.html#un-nom-de-domaine).

> Mon fournisseur d’accès ne me donne pas une adresse ip fixe.

C’est effectivement embêtant. Car dans ce cas, votre nom de domaine
“machintruc.com” doit régulièrement être mis à jour pour pointer
vers votre nouvelle adresse ip.

Sachez qu’il existe pour ça DynDNS. C’est un service qui permet de
réaliser cette opération, et la plupart des \*box permettent de le
configurer.

Cela peut toutefois être fastidieux, et vous préférerez sûrement
changer de fournisseur d’accès pour un autre respectueux de votre
vie privée ([FDN](http://www.fdn.fr) par exemple).

> Où trouver des applications pour mon serveur?

Le site [WAAH](http://waah.quent1.fr/), Wiki des Applications
Auto-Hébergées recense de nombreuses applications que l’on peut
héberger sur son serveur. Il en existe sûrement d’autres, mais
celui-ci est particulièrement complet.

Il existe aussi [alternativeto](http://alternativeto.net/platform/self-hosted/) qui recense
quelques projets.

Exemples de matériel pour un serveur
------------------------------------

Voici quelques propositions de matériel pouvant être utilisé comme
serveur si vous souhaitez l’acheter.

-   [raspberry pi](https://www.raspberrypi.org/) : déjà évoqué dans ce
    document, le raspberry pi est très populaire et donc largement
    documenté. Bien qu’il consomme très peu de ressources, certains lui
    reprocheront l’absence de port pour y brancher un disque dur. Notez
    que vous pouvez toutefois y ajouter un disque dur externe via usb.
-   [Un banana pi](http://www.armbian.com/banana-pi/) : c’est une
    version améliorée du raspberry pi, et donc un peu plus cher. Vous
    pourrez y brancher directement un disque dur.
-   Le [cubieboard](http://cubieboard.org/) suit un peu le même principe
    que les suggestions précédentes.
-   Les [fit-pc](http://www.fit-pc.com/web/) sont encore plus onéreux
    mais font office de matériel plus robuste. Ils disposent d’un disque
    dur intégré, tout en restant à faible consommation électrique.
-   Les cartes [alix](http://www.pcengines.ch/alix.htm) sont encore
    moins gourmandes en énergie, mais nécessitent des connaissances
    supplémentaires pour y installer un système.

Dans tous les cas, gardez bien en tête qu’un vieux pc fera très bien
l’affaire plutôt que le jeter.

Notes à propos du raspberry pi
------------------------------

Un [raspberrypi](http://raspberrypi.org) est une minuscule machine très
peu gourmande en électricité.. C’est certainement une des solutions les
plus économiques, que ce soit du point de vue matériel, prix et
consommation.

Pour l’utiliser comme serveur, vous aurez besoin :

-   Un raspberry pi
-   Une carte SD
-   Un cable ethernet.
-   Un disque dur externe qui a sa propre alimentation électrique (si
    votre serveur sert de stockage)

### Raspbian

[Raspbian](http://raspbian.org) est une distribution linux dérivée de
[debian](http://debian.org) et optimisée pour le raspberry pi. Je vous
la recommande. Vous pouvez télécharger l’image de raspbian sur ce site :
<http://www.raspberrypi.org/downloads>.

À partir d’une distribution linux, elle se copie sur la carte SD avec
cette simple commande :

`dd if=/chemin/vers/raspbian.img of=/dev/sdb`

À condition que /dev/sdb représente bien votre carte SD. Pour
être sûr, tapez la commande `dmesg` juste après avoir branché la carte
SD, ou bien `fdisk -l` pour lister tous les périphériques branchés.

Voyons maintenant comment obtenir une installation minimale de raspbian.

### Installateur minimal ua-netinst (méthode 1)

Un installateur est disponible pour avoir une raspbian minimale. C’est
la méthode que la meilleure pour avoir une installation de raspbian la
plus légère possible.

L’installateur se trouve ici :
<https://github.com/debian-pi/raspbian-ua-netinst/releases/latest>.

Une fois une des archive téléchargée, voici la marche à suivre pour
l’installer :

1.  Décompresser l’archive : | unzx raspbian-ua-netinst-v1.0.7.Img.xz |
2.  Copier l’image sur la carte SD :
    `dd if=raspbian-ua-netinst-v1.0.7.Img of=/dev/sdX`. Remplacez
    /dev/sdX par le nom de votre carte SD comme indiqué ci-dessus.
3.  Insérez la carte SD dans le raspberry pi, puis branchez le cable
    ethernet. Enfin, allumez-le. L’installation se déroule
    tranquilement.
4.  Au premier démarrage, modifiez le mot de passe du compte root avec
    la commande `passwd`.
    Par défaut, le mot de passe de **root** est **raspbian**.
5.  Changez la langue par défaut : `dpkg-reconfigure locales`
6.  Changez la zone horaire : `dpkg-reconfigure tzdata`
7.  Lisez le README :
    <https://github.com/debian-pi/raspbian-ua-netinst/blob/master/README.md>

Et voilà!

### Préparation de l’image avec qemu (méthode 2)

Si on souhaite utiliser le raspberry pi comme serveur, il faut se
débarrasser de tout ce qui est inutile.

Si vous n’avez pas d’écran pour configurer raspbian après l’avoir mis
sur la carte SD, ou si vous voulez préparer la distribution avant de la
copier, vous pouvez utiliser *qemu*.

Cet outil va démarrer sur l’image de raspbian, comme si elle était
installée sur le raspberry pi, avant de pouvoir copier l’image modifiée
sur la carte SD du RPi.

Vous aurez besoin de télécharger [un kernel spécifique à
qemu](http://yeuxdelibad.net/DL/kernel-3.6.8-armhf-qemu.zip).

Installez qemu-system sur votre distribution linux, puis lancez l’image
de raspbian avec cette commande :

```
qemu-system-arm -kernel kernel-3.6.8-armhf-qemu -cpu \
    arm1176 -m 512 -M versatilepb -no-reboot -serial stdio\
    -append "root=/dev/sda2 panic=1" -hda NOMDELIMAGE.img
```

Si ça ne fonctionne pas, essayez avec celle-ci, qui ne lance pas
d’interface graphique :

```
qemu-system-arm -M versatilepb -cpu arm1176 -hda NOMDELIMAGE.img \
-nographic -no-reboot -kernel kernel-3.6.8-armhf-qemu \
-append "root=/dev/sda2 panic=1 console=ttyAMA0"
```

Connectez-vous avec les identifiants suivants :

-   login : pi
-   password : raspberry

Ensuite, vous pourrez alors vous débarasser de tout ce qui vous est
inutile (lxde, pcmanfm, dillo, openbox, idle ...), et installer ssh avec
la commande `apt-get` bien sûr.

Pour arrêter, tapez `halt`.

Vous obtenez ainsi une image de rasbian personnalisée, qui peut être
copiée sur la carte SD.

### Faire le ménage

Pour retirer une bonne partie des paquets inutiles pour un serveur, ces
commandes feront l’affaire :

```
# apt-get remove --purge desktop-base \
x11-common midori omxplayer scratch dillo xpdf galculator \
netsurf-common netsurf-gtk lxde-common lxde-icon-theme \
hicolor-icon-theme libpoppler19 ed lxsession lxappearance lxpolkit \
lxrandr lxsession-edit lxshortcut lxtask lxterminal xauth \
debian-reference-common fontconfig fontconfig-config \
fonts-freefont-ttf wolfram-engine dbus-x11 desktop-file-utils \
libxmuu1
# apt-get autoremove
# rm -rf /opt/* \
/usr/share/icons/* \
/usr/games \
/usr/share/squeak \
/usr/share/sounds \
/usr/share/wallpapers \
/usr/share/themes \
/usr/share/kde4 \
/usr/share/images/* \
/home/pi/python_games
```

### Augmenter la taille de l’image avec qemu-img

Si vous ajoutez de nombreux paquets à l’image ci-dessus, elle sera
sûrement trop petite. Pour résoudre ce problème, cette commande
agrandira l’image :

```
qemu-img resize raspbian.img +2GB
```

Ensuite, démarrer l’image avec qemu, puis connectez-vous avec le compte
pi. On va maintenant copier l’utilitaire raspi-config pour le modifier
légèrement. Lancez ces quelques commandes :

```
cp /usr/bin/raspi-config ~
sed -i 's/mmcblk0p2/sda2/' ~/raspi-config
sed -i 's/mmcblk0/sda/' ~/raspi-config
sudo ~/raspi-config
```

Choisissez `expand_rootfs`. Redémarrez, et regardez le résultat en
tapant `df`.

